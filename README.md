# Project Description
This project was created as an introductory exercise to learn about the React JavaScript library, JSX and ES6. 
The result is a fully functional stopwatch with start, stop, and reset controls. It also has
the ability to track a history of laps.

## Directions

Install npm, yarn, and watchman
````
> yarn install # installs dependencies
> yarn start   # starts the development server and watch files
> yarn build   # creates an optimized build 
````

## Uses
- [Material-UI](https://material-ui.com/)  
- [Create React App](https://github.com/facebookincubator/create-react-app)

### Building Watchman for Ubuntu 18.04
You need at least watchman version **v4.9.0** to work with Ubuntu 18.04. If you are using Ubuntu 16.04, then 
make sure you checkout **v4.7.0** instead. 

````
> git clone https://github.com/facebook/watchman.git
> cd watchman/
> git checkout v4.9.0
> sudo apt-get install -y autoconf automake build-essential python-dev libssl-dev libtool pkg-config
> ./autogen.sh
> ./configure
> make
> sudo make install

````